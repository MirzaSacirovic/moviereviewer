@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <p class="lead">Movies/TV Shows</p>
            <label class="switch">
              <input type="checkbox" id="toggle" value="1">
              <span class="slider round"></span>
            </label>
        </div>
            <div class="col-md-9">
                <div class="row" style="margin-bottom:30px;">
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                           
                                <div class="carousel-inner">
                                    <?php $i=0;  ?>
                                    @foreach($movies as $movie)
                                    @if($i == 0)
                                          <div class="item active">
                                            <a href="{{route('show', [$movie->slug])}}"><img alt="First slide" src="data:image/jpeg;base64,{{base64_encode($movie->photo)}}" style="width: 100%;"></a>
                                          </div>
                                    <?php $i=1;  ?>
                                    @else
                                        <div class="item">
                                            <a href="{{route('show', [$movie->slug])}}"><img alt="First slide" src="data:image/jpeg;base64,{{base64_encode($movie->photo)}}" style="width: 100%;"></a>
                                          </div>
                                    @endif
                                  @endforeach
                                </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>

            @if (count($movies) > 0)
                <section class="movies">
                    @include('load')
                </section>
            @endif
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            </div>
              </div>
        </div>
      

@stop