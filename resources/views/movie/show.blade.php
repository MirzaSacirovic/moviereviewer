@extends('layout')
@section('content')
              <img src="data:image/jpeg;base64,{{base64_encode($movie->photo)}}" alt="">
              <div class="caption">
                  <h4><a href="{{route('show', [$movie->slug])}}">{{$movie->title}}</a></h4></br>
                  <p>{{$movie->description}}</p>
              </div>
              <div class="actors">
                <h4>Actors:</h4>
                @foreach($actors as $actor)
                  <p>{{$actor->first_name}} {{$actor->last_name}}</p>
                @endforeach
              </div>
              <div class="ratings">
                 <div>
                  <p>Rating: {{ number_format($movie->rating_cache, 2) }}</p>
                </div>
                <div>
                  <p>People rated: {{$movie->rating_count}}</p>
                </div>
                <p>Rate me:</p>
                <p>
                  @for ($i=1; $i <= 5 ; $i++)
                    <input id="movie_id" type="hidden" name="movie_id" value="{{$movie->id}}">
                    <button id="rating_show" type="submit" class="btn-glyphicon glyphicon glyphicon-star{{ ($i <= $movie->rating_cache) ? '' : '-empty'}}" value="{{$i}}" name=rating></button>
                  @endfor
                </p>
            </div>
@stop