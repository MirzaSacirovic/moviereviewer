<div class="row" id="load-data">
    <div id=display>
        @foreach($movies as $movie)
            <div class="col-sm-4 col-lg-4 col-md-4">
                <div class="thumbnail">
                    <img src="data:image/jpeg;base64,{{base64_encode($movie->photo)}}" alt="">
                    <div class="caption">
                        <h4><a href="{{route('show', [$movie->slug])}}">{{$movie->title}}</a></h4></br>
                        <h4 class="pull-right">{{ number_format($movie->rating_cache, 2) }}</h4>
                        <p>{{$movie->description}}</p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">{{$movie->rating_count}}</p>
                        <p>
                            @for ($i=1; $i <= 5 ; $i++)
                                <input id="movie_id" type="hidden" name="movie_id" value="{{$movie->id}}">
                                <button id="rating_load" type="submit" class="btn-glyphicon glyphicon glyphicon-star{{ ($i <= $movie->rating_cache) ? '' : '-empty'}}" value="{{$i}}" name=rating></button>
                            @endfor
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

<div id="remove-row">
    <button id="btn-more" data-id="{{ $movie->rating_cache }}" class="nounderline btn-block mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" > Load More </button>
</div>