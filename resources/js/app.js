
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(function() {
   //On pressing a key on "Search box" in "index.blade.php" file. This function will be called.
   $("#search").keyup(function() {
        //Assigning search box value to javascript variable named as "name" and toggle to "toggle".

       var name = $('#search').val();
       var toggle = $('#toggle').val();
       if(toggle == 1){
        toggle = 0;
       }
       else{
        toggle = 1;
       }
    //check if empty

        if(this.value.length == 0){
          $.ajax({
               //AJAX type is "Post".
               type: "POST",
               //Data will be sent to "search".
               url: "search",
               //Data, that will be sent to "search".
               data: {
                   //Assigning value of "name" into "search" variable.
                   search: name, toggle: toggle, "_token": $('#token').val()
               },
               //If result found, this funtion will be called.
               success: function(data) {
                   //Assigning result to "display" div.
                  $('#display').empty();
                  $('#remove-row').remove();
                  $('#display').append(data);
               }
           });
        }
    //check if it's more than 3 letters
       if( this.value.length < 3){
        return;
       } 
    
           //AJAX is called.
           $.ajax({
               //AJAX type is "Post".
               type: "POST",
               //Data will be sent to "search".
               url: "search",
               //Data, that will be sent to "search".
               data: {
                   //Assigning value of "name" into "search" variable.
                   search: name, toggle: toggle, "_token": $('#token').val()
               },
               //If result found, this funtion will be called.
               success: function(data) {
                   //Assigning result to "display" div.
                  $('#display').empty();
                  $('#remove-row').remove();
                  $('#display').append(data);
               }
           });
   });
});

$(document).ready(function(){
   //On pressing a buttom on "btn more" in "index.blade.php" file. This function will be called.

   $(document).on('click','#btn-more',function(){
       //Assigning btn value to javascript variable named as "id".

       var id = $(this).data('id');
       $("#btn-more").html("Loading....");
       //Assigning toggle value to javascript variable named as "toggle".

       var toggle = $('#toggle').val();

       //switch toggle to opposite since it already changed in previous script

       if(toggle == 1){
        toggle = 0;
       }
       else{
        toggle = 1;
       }

           //AJAX is called.
       $.ajax({
               //Data will be sent to "loaddata".
           url : "loaddata",
               //AJAX type is "Post".
           method : "POST",
               //Data, that will be sent to "search".
           data : {id:id, toggle:toggle, "_token": $('#token').val()},
           dataType : "text",
               //If result found, this funtion will be called.

           success : function (data)
           {
              if(data != '') 
              {
                //fill results and remove old button
                  $('#remove-row').remove();
                  $('#load-data').append(data);
              }
              else
              {
                //no more results

                  $('#btn-more').html("No Data");
              }
           }
       });
   });  

   //On pressing a toggle in "index.blade.php" file. This function will be called.

   $(document).on('click','#toggle',function(){
       //Assigning toggle value to javascript variable named as "toggle".

       var toggle = $('#toggle').val();
          //AJAX is called.
       $.ajax({
               //Data will be sent to "switch".
           url : "switch",
               //AJAX type is "Post".
           method : "POST",
               //Data, that will be sent to "switch".
           data : {toggle:toggle, "_token": $('#token').val()},
           dataType : "text",
               //If result found, this funtion will be called.
           success : function (data)
           {
              if(data != '') 
              {
                //empty old html append new data

                  $('#display').empty();
                  $('.new-data').remove();
                  $('#remove-row').remove();
                  $('#display').append(data);
              }
           }
       });
       //switch toggle value
       if(toggle == 1){
        $('#toggle').val(0);
        var toggle = $('#toggle').val();

       }
       else{
        $('#toggle').val(1);
        var toggle = $('#toggle').val();
       }

       
   });  
}); 

$(document).on('click','#rating_load',function(){
	var url = window.location.protocol + "//" + window.location.host + "/";
   	movie_id = $("#movie_id").val();
   	rating = this.value;

      //AJAX is called.
   	$.ajax({
           //Data will be sent to "store-review".
       url : url + "movies/store-review",
           //AJAX type is "Post".
       method : "POST",
           //Data, that will be sent to "store-review".
       data : {movie_id:movie_id, rating:rating, "_token": $('#token').val()},
       dataType : "text",
           //If result found, this funtion will be called.
       success : function (data)
       {
       	 location.reload();
       }
   	});
});

$(document).on('click','#rating_show',function(){
	var url = window.location.protocol + "//" + window.location.host + "/";
   	movie_id = $("#movie_id").val();
   	rating = this.value;

      //AJAX is called.
   	$.ajax({
           //Data will be sent to "store-review".
       url : url + "movies/store-review",
           //AJAX type is "Post".
       method : "POST",
           //Data, that will be sent to "store-review".
       data : {movie_id:movie_id, rating:rating, "_token": $('#token').val()},
       dataType : "text",
           //If result found, this funtion will be called.
       success : function (data)
       {
       	 location.reload();
       }
   	});
});  