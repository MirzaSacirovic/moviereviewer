<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;
use App\Models\Actor;

class AddTwiceActorMovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $movies = Movie::all();
        foreach ($movies as $movie) {
        	
            $actor = Actor::inRandomOrder()->first();
            $movie->actor()->attach($actor->id);
        }
    }
}
