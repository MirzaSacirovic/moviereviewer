<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;
use App\Models\Actor;
use Carbon\Carbon;

class MovieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
		$date = Carbon::create(2015, 5, 28, 0, 0, 0);

        for ($i = 0; $i < 50; $i++) {
            Movie::create([
                'rating_cache' => rand(0,5),
                'rating_count' => rand(0,100),
                'title' => $faker->sentence,
                'slug' => str_slug($faker->sentence,'-'),
                'description' => $faker->paragraph,
                'release_date' => $date->addWeeks(rand(1,52))->format('Y-m-d H:i:s'),
                'photo' => file_get_contents( $faker->image('public/images',800,200) )
            ]);
        }
        $movies = Movie::all();
        foreach ($movies as $movie) {
        	
            $actor = Actor::inRandomOrder()->first();
            $movie->actor()->attach($actor->id);
        }
    }
}
