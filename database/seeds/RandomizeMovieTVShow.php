<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;

class RandomizeMovieTVShow extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = Movie::all();
        foreach ($movies as $movie) {
        	$movie->tv_show = rand(0,1);
        	$movie->save();
        }
    }
}
