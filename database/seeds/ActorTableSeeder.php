<?php

use Illuminate\Database\Seeder;
use App\Models\Actor;

class ActorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Actor::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName
            ]);
        }
    }
}
