<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;

    public $table = 'movies';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
            'photo',
            'rating_cache',
            'rating_count',
            'title',
            'description',
            'release_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'photo' => 'binary',
        'rating_cache' => 'float',
        'rating_count' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'release_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
            
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function movieactor()
    {
        return $this->hasMany(\App\Models\MovieActor::class);
    }

      /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function actor()
    {
         return $this->belongsToMany('\App\Models\Actor', 'movie_actor', 'movie_id', 'actor_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function review()
    {
        return $this->hasMany(\App\Models\Review::class);
    }

    //function to calculate rating after adding new review
     public function recalculateRating(){
        $reviews = $this->review();
        $avgRating = $reviews->avg('rating');
        $this->rating_cache = round($avgRating,1);
        $this->rating_count = $reviews->count();
        $this->save();
    }
}
