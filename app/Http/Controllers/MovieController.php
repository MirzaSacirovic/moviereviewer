<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Flash;
use Response;
use App\Models\Movie;
use App\Models\Review;
use App\Models\Actor;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class MovieController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Display all of the movies.
     */
    public function index()
    {
    	$movies = Movie::where('tv_show', '=', 0)->orderBy('rating_cache','desc')->limit(10)->get();
    	
        return view('index')
            ->with(compact('movies', 'tvshows'));
    }
    /**
     * Display the specific movie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($slug)
    {
        $movie = Movie::where('slug', 'LIKE', $slug)->first();

        $actors = Actor::join('movie_actor', 'movie_actor.actor_id', '=', 'actors.id')->join('movies', 'movies.id', '=', 'movie_actor.movie_id')->where('movie_actor.movie_id', '=', $movie->id)->get();

        if (empty($movie)) {
            Flash::error('Movie not found');

            return redirect(route('movies.index'));
        }

        return view('movie.show')->with(compact('movie', 'actors'));
    }

     //new review
    public function storeReviewForMovie(Request $request)
    {
    	$movie_id = $request->input('movie_id');

    	$rating = $request->input('rating');

        $movie = Movie::find($movie_id);

        //get authenticated user if any
        if(Auth::user()){
            $user_id = Auth::user()->id;
        }

        $review = new Review();
        if(isset($user_id)){
            $review->user_id = $user_id;
        }
        $review->movie_id = $movie_id;
        $review->rating = $rating;
        $movie->review()->save($review);

        // recalculate ratings for the specified movie
        $movie->recalculateRating();

        return response()->json(['success'=>'Score updated'], 200); 
    }

    //ajax for load more
    public function loaddata(Request $request)
	{
		//get data from request
		$id = $request->id;
		$toggle = $request->toggle;
		//query DB for movies
		$movies=Movie::where('tv_show', '=', $toggle)->where('rating_cache', '<', $id)->orderBy('rating_cache','desc')->limit(10)->get();
		if($movies){
			//if found results return partial view for ajax
			return view('load')->with(compact('movies'))->render();
	   	}
	}

    //ajax for load switch tv show/ movie

	public function switch(Request $request)
	{
		//get data from request
		$toggle = $request->toggle;
		//get data about movies from DB
		$movies=Movie::where('tv_show', '=', $toggle)->orderBy('rating_cache','desc')->limit(10)->get();
		if($movies){
			//if found results return partial view for ajax
			return view('load')->with(compact('movies'))->render();
	   	}
	}

	public function search(Request $request)
	{
		//get data from request
		$search = $request->search;

		$toggle = $request->toggle;

		if($search == ""){
			$movies=Movie::where('tv_show', '=', $toggle)->orderBy('rating_cache','desc')->limit(10)->get();
		}

		//check input string for complex search
		if(strpos($search, 'after') !== false || strpos($search, 'older') !== false){
			//explode element to get the year
			$year = explode(' ', $search);
			foreach ($year as $yr) {
				if(is_numeric($yr)){
					$movies=Movie::where('tv_show', '=', $toggle)->whereYear('release_date', '>', $yr)->orderBy('rating_cache','desc')->limit(10)->get();
				}
			}
		}

		//check if before
		if(strpos($search, 'before') !== false || strpos($search, 'younger') !== false){
			//explode element to get the year
			$year = explode(' ', $search);
			foreach ($year as $yr) {
				if(is_numeric($yr)){
					$movies=Movie::where('tv_show', '=', $toggle)->whereYear('release_date', '<', $yr)->orderBy('rating_cache','desc')->limit(10)->get();
				}
			}
		}

		//check for stars
		if(strpos($search, 'star') !== false){
			//check if it's more
			if(strpos($search, 'more')){
				//explode element to get the value
				$stars = explode(' ', $search);
				foreach ($stars as $star) {
					if(is_numeric($star)){
						$movies=Movie::where('tv_show', '=', $toggle)->where('rating_cache', '>', $star)->orderBy('rating_cache','desc')->limit(10)->get();
					}
				}
			}

			//check if it's atleast
			if(strpos($search, 'least') !== false){
				//explode element to get the value
				$stars = explode(' ', $search);
				foreach ($stars as $star) {
					if(is_numeric($star)){
						$movies=Movie::where('tv_show', '=', $toggle)->where('rating_cache', '>=', $star)->orderBy('rating_cache','desc')->limit(10)->get();
					}
				}
			}

			//check if less
			if(strpos($search, 'less') !== false){
				//explode element to get the value
				$stars = explode(' ', $search);
				foreach ($stars as $star) {
					if(is_numeric($star)){
						$movies=Movie::where('tv_show', '=', $toggle)->where('rating_cache', '<', $star)->orderBy('rating_cache','desc')->limit(10)->get();
					}
				}
			}

			if(empty($movies)){
				//explode element to get the value
				$stars = explode(' ', $search);
				foreach ($stars as $star) {
					if(is_numeric($star)){
						$movies=Movie::where('tv_show', '=', $toggle)->where('rating_cache', '=', $star)->orderBy('rating_cache','desc')->limit(10)->get();
					}
				}	
			}
		}

		if(empty($movies)){
			$movies=Movie::where('tv_show', '=', $toggle)->where('title', 'LIKE', '%'.$search.'%')->orWhere('description', 'LIKE', '%'.$search.'%')->orderBy('rating_cache','desc')->limit(10)->get();
		}

		//get data about movies from DB
		if($movies){
			//if found results return partial view for ajax
			return view('load')->with(compact('movies'))->render();
	   	}
	}


    //function to recalculate ratings 
    public function recalculateRating(Request $request)
    {
    	$id = $request->input('id');
    	
        $movie = Movie::find($id);

        $movie->recalculateRating();

        return response()->json(['success'=>'Movie rating updated'], 200); 
        
    }

}
