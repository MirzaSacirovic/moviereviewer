<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MovieController@index');

Route::post('loaddata', 'MovieController@loaddata')->name('loaddata');
Route::post('switch', 'MovieController@switch')->name('switch');
Route::get('show/{slug}', 'MovieController@show')->name('show');
Route::post('search', 'MovieController@search')->name('search');
Route::post('movies/store-review', 'MovieController@storeReviewForMovie')->name('storereview');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
